package domain;

import service.UserService;
import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args){

        UserService userService = new UserService();
        User user = new User();
        User user1 = new User();
        User user2 = new User();

        Address address1 = new Address();
        Address address2 = new Address();
        user.setName("adrian");
        user1.setName("Krzysztof");
        user2.setName("Tom");

        List<Address> addressList = new LinkedList<>();
        addressList.add(address1);
        addressList.add(address2);

        List<Address> addressList1 = new LinkedList<>();
        addressList1.add(address1);

        address2.setCity("Wroclaw");
        address2.setHouseNumber(30);

        address1.setCity("Warszawa");
        address1.setHouseNumber(10);
        address1.setFlatNumber(12);

        Person person1 = new Person();
        Person person2 = new Person();

        person1.setAddresses(addressList);
        person2.setAddresses(addressList1);
        user.setPersonDetails(person1);
        user.setPersonDetails(person2);

        List <User> userList = new LinkedList<>();
        userList.add(user);
        userList.add(user1);
        userList.add(user2);

        //System.out.println(userList.get(0));
        //System.out.println(userService.findUsersWhoHaveMoreThanOneAddress(userList));

        userService.findUserWithLongestUsername(userList);




    }
}
