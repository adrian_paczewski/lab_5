package service;

import domain.Person;
import domain.Role;
import domain.User;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;



public class UserService {


    public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {

        List<User> moreThanOneAddress = users.stream()
                .filter(u -> u.getPersonDetails().getAddresses().size() > 1)
                .collect(Collectors.toList());
        return moreThanOneAddress;
    }


    public static Person findOldestPerson(List<User> users) {

        Person oldest =  users.stream()
                .max(Comparator.comparing(Person::getAge))
                .get();
        return oldest;
    }


    public static User findUserWithLongestUsername(List<User> users) {

        User longestUsername = users.stream()
                .max(Comparator.comparing(User::getName))
                .get();
        return longestUsername;
    }


    public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {

        String namesAndSurnames = users.stream()
                .filter(u -> u.getPersonDetails().getAge() > 18)
                .map(user -> user.getPersonDetails().getName())
                .collect(Collectors.joining(","));

            return namesAndSurnames;
    }


    public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {

        List<User> sortedList = users.stream()
                .filter(u -> u.getPersonDetails().getRole().getName().startsWith("A"))
                .sorted()
                .collect(Collectors.toList());

        return sortedList;
    }


    public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {

        users.stream()
                .filter(u -> u.getPersonDetails().getRole().getName().startsWith("S"))
                .collect(Collectors.toList())
                .forEach(System.out::println);


    }


    public static Map<Role, List<User>> groupUsersByRole(List<User> users) {

        Map <Role, List<User>> roleGrouped = users.stream()
                .collect(Collectors.groupingBy(user -> user.getPersonDetails().getRole()));

        for(Map.Entry entry : roleGrouped.entrySet()) {
            System.out.println(entry.getKey() + ", " + entry.getValue());
        }

        return roleGrouped;
    }


    public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {
        Map<Boolean, List<User>> userMap = users.stream()
                .collect(Collectors.partitioningBy(u -> u.getPersonDetails().getAge() > 18));

        return userMap;
    }
}
